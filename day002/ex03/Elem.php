<?php

class Elem {

	private $element;
	private $content;
	private $innerElem = array();
	private $supported = array("meta", "img", "hr", "br", "html", "head", "body", "title", "h1", "h2", "h3", "h4", "h5", "h6", "p", "span", "div");

	function __construct($element, $content = null) {
	
		if (in_array($element, $this->supported) == FALSE) {
			die("The class support the following HTML tags:\n
					meta\nimg\nhr\nbr\nhtml\nhead\nbody\ntitle\nh1\nh2\nh3\nh4\nh5\nh6\np\nspan\ndiv\n.
					Your tag $element is not in this list.\n");
		}
		$this->element = $element;
		$this->content = $content;
	
	}

	function pushElement(Elem $newElem) {
		
		if ($newElem)
			array_push($this->innerElem, $newElem);
	}

	function getHTML($i = 0) {
		
		$tabs = "";
		for ($index = 0; $index < $i; $index++)
			$tabs .= "    ";
		$htmltxt .= "$tabs<$this->element>\n";
		if ($this->content)
			$htmltxt .= "$tabs    $this->content\n";

		$i++;
		if (count($this->innerElem) > 0)
			foreach ($this->innerElem as $el) {
				$htmltxt .= $el->getHTML($i);
			}
		$htmltxt .= "$tabs</$this->element>\n";

		return $htmltxt;
	
	}

}

?>
