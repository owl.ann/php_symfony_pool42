<?php

include ('./Text.php');

class TemplateEngine {

	public function createFile($fileName, Text $text) {

		file_put_contents($fileName, "");

		$htmltxt = "<!DOCTYPE>\n";
		$htmltxt .= "<html>\n";
		$htmltxt .= "	<head>\n";
		$htmltxt .= "		<title>text</title>\n";
		$htmltxt .= "	</head>\n";
		$htmltxt .= "	<body>\n";

		$htmltxt .= $text->getBody();

		$htmltxt .= "	</body>\n";
		$htmltxt .= "</html>\n";

		file_put_contents($fileName, $htmltxt);
	}

}

?>
