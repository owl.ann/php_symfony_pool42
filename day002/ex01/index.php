<?php

include('./TemplateEngine.php');

$template = new TemplateEngine();
$text = new Text(array("Modify the <bold>TemplateEngine</bold> class's <bold>createFile(\$fileName, Text \$text)</bold> method to take a <bold>Text</bold> object as the last argument.", "The <bold>Text</bold> class sholud have a constructor which takes an arrayof strings, a method for adding new strings to it and a method to render all the strings as HTML, each encased in a <bold> p </bold> tag."));

$text->addString("The <bold>createFile</bold> method should create an HTML sttic similar to the one generated in <bold>Exercise 00</bold> and the body should contain the rendered content of the Text class.");

$text->addString(array("Just", "checking"));
$text->renderHtml();

$template->createFile("newFile.html", $text);

?>
