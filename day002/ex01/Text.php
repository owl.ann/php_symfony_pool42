<?php

class Text {

	private $arrayOfStrings = array();

	function __construct($strings) {
		$this->$arrayOfStrings = $strings;
	}

	function addString($str) {
		if (is_string($str))
			array_push($this->$arrayOfStrings, $str);
		else if (is_array($str))
			$this->$arrayOfStrings = array_merge($this->$arrayOfStrings, $str);
	}

	function renderHtml() {

		foreach ($this->$arrayOfStrings as $key => &$value) {
			$value = '<p>'. $value . '<p>';
		}

	}

	function getBody() {

		$txt = "";
		foreach ($this->$arrayOfStrings as $value) {
			$txt .= $value . "\n";
		}
		return $txt;
	}
}

?>
