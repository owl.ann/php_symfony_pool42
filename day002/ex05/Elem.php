<?php

class Elem {

	private $element;
	private $content;
	private $attributes;
	private $file;
	private $innerElem = array();
	private $supported = array("meta", "img", "hr", "br", "html", "head", "body", "title", "h1", "h2", "h3", "h4", "h5", "h6", "p", "span", "div", "table", "tr", "th", "td", "ul", "ol", "li");
	private static $counter = 0;

	function __construct($element, $content = null, $attributes = null) {

		try {
		if (in_array($element, $this->supported) == FALSE) {
			throw new MyException("The class support the following HTML tags:\n"
					. "meta\nimg\nhr\nbr\nhtml\nhead\nbody\ntitle\nh1\nh2\nh3\nh4\nh5\nh6\np\nspan\ndiv\n"
					. "Your tag $element is not in this list.\n");
		}
		$this->element = $element;
		$this->content = $content;
		$this->attributes = $attributes;
		} catch (MyException $e) {
			echo $e->getMessage();
		}
	
	}

	function getElement() {
		return $this->element;
	}

	function getAttributes() {
		return $this->attributes;
	}
	
	function checkHtml() {
		if ($this->element != "html")
			return false;
		if ($this->innerElem[0]->getElement() != "head")
			return false;
		if ($this->innerElem[1]->getElement() != "body")
			return false;
		return true;
	}

	function checkHead() {
		if ($this->element != "head")
			return false;
		if (count($this->innerElem) != 2)
			return false;
		foreach ($this->innerElem as $val) {
			if ($val->getElement() != "title" && ($val->getElement() != "meta" 
				|| count(($attribute = $val->getAttributes())) != 1 || key($attribute) != "charset"))
				return false;
		}
		return true;
	}

	function checkP() {
		echo "count: " . count($this->innerElem) . "\n";
		if (count($this->innerElem) > 0)
			return false;
	}

	function checkTable() {
		foreach ($this->innerElem as $val) {
			if ($val->getElement() != "tr")
				return false;
		}
	}

	function checkTr() {
		foreach ($this->innerElem as $val) {
			if ($val->getElement() != "th" && $val->getElement() != "td")
				return false;
		}
		return true;
	}

	function checkList() {
		foreach ($this->innerElem as $val) {
			if ($val->getElement() != "li")
				return false;
		}
		return true;
	}

	function validPage() {

		if (Elem::$counter == 0) {
			if (!$this->checkHtml())
				return false;
			Elem::$counter = 1;
			if (!$this->innerElem[0]->validPage())
				return false;
		} else if (Elem::$counter == 1) {
			if (!$this->checkHead())
				return false;
			Elem::$counter = 2;
		} else if (Elem::$counter == 2) {
			Elem::$counter = 3;
			if (!$this->validPage())
				return false;
		} else {
			foreach ($this->innerElem as $val) {
				if (!$val->validPage())
					return false;
			}

			if ($this->element == "p") {
				if (!$this->checkP())
					return false;
			} else if ($this->element == "table") {
				if (!$this->checkTable())
					return false;
			} else if ($this->element == "tr") {
				if (!$this->checkTr())
					return false;
			} else if ($this->element == "ul" || $this->element == "ol") {
				if (!$this->checkList())
					return false;
			}
			Elem::$counter = 0;
		}
		return true;
	}

	function pushElement(Elem $newElem) {
		
		if ($newElem)
			array_push($this->innerElem, $newElem);
	}

	function getHTML($i = 0) {
		
		$tabs = "";
		for ($index = 0; $index < $i; $index++)
			$tabs .= "    ";

		if ($this->element)	 {
			$htmltxt .= "$tabs<$this->element";
			if ($this->attributes)
				foreach ($this->attributes as $key => $value) {
					$htmltxt .= " $key=\"$value\"" ;
				}
			$htmltxt .= ">\n";
	
			if ($this->content)
				$htmltxt .= "$tabs    $this->content\n";

			$i++;
			if (count($this->innerElem) > 0)
				foreach ($this->innerElem as $el) {
					$htmltxt .= $el->getHTML($i);
				}
			$htmltxt .= "$tabs</$this->element>\n";
		}

		return $htmltxt;
	
	}

}

?>
