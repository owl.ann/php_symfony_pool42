<?php

include ('./Elem.php');
include ('./TemplateEngine.php');
include ('./MyException.php');

$elem = new Elem('html');
$head = new Elem('head');
$head->pushElement(new Elem('title', 'test'));
$head->pushElement(new Elem('meta', "", ['charset' => 'UTF-8']));
$body = new Elem('body');
$body->pushElement(new Elem('p', 'Lorem ipsum', ["style" => "color:pink;", "class" => "text-muted"]));
$elem->pushElement($head);
$elem->pushElement($body);

$template = new TemplateEngine($elem);
$template->createFile("Lorem.html");

echo (int)$elem->validPage() . "\n";

$elem = new Elem('html');
$head = new Elem("head");
$body = new Elem("body");

$elem->pushElement($body);
$elem->pushElement($head);

echo (int)$elem->validPage() . "\n";

$elem = new Elem('html');
$head = new Elem('head');
$head->pushElement(new Elem('title', 'test'));
$head->pushElement(new Elem('meta', "", ['charset' => 'UTF-8']));
$body = new Elem('body');
$body->pushElement(new Elem('p', 'Lorem ipsum', ["style" => "color:pink;", "class" => "text-muted"]));
$elem->pushElement($head);
$elem->pushElement($body);
$p = new Elem('p');
$p->pushElement(new Elem('p'));
$body->pushElement($p);

echo (int)$elem->validPage() . "\n";

?>
