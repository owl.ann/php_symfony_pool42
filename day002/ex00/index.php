<?php

include('./TemplateEngine.php');

$template = new TemplateEngine();

$template->createFile("JoyFielding.html", "book_description.html", array("nom" => "All the wrong places", "auteur" => "Joy Fielding", "description" => "Four women--friends, family, rivals--turn to online dating for companionship, only to find themselves in the crosshairs of a tech-savvy killer using an app to target his victims in this harrowing thriller from the New York Times bestselling author of See Jane Run and The Bad Daughter.", "prix" => "17$"));

$template->createFile("AJKazinski.html", "book_description.html", array("nom" => "The last good man", "auteur" => "A. J. Kazinski", "description" => "In Jewish scripture, there is a legend: There are thirty-six righteous people on earth. The thirty-six protect us. Without them, humanity would perish. But the thirty-six do not know they are the chosen ones.", "prix" => "14$"));


?>
