<?php

class TemplateEngine {

	public function createFile($fileName, $templateName, $parameters) {

		file_put_contents($fileName, "");
		if ($templateName && ($file = fopen($templateName, "r"))) {
			while (($str = fgets($file))) {
					foreach ($parameters as $key => $value) {
						if (strpos($str, $key) !== FALSE) {
							$str = str_replace("{" . $key . "}", $value, $str);
							break;
						}
					}
					file_put_contents($fileName, $str, FILE_APPEND);
			}
		}

	}

}

?>
