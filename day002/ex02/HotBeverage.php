<?php

class HotBeverage {

	public $name;
	public $price;
	public $resistence;

	function getName() {
		return $this->name;
	}

	function setName($val) {
		$this->name = $value;

		return $this;
	}

	function getPrice() {
		return $this->price;
	}

	function setPrice($val) {
		$this->price = $val;

		return $this;
	}

	function getResistence() {
		return $this->resistence;
	}

	function setResistence($val) {
		$this->resistence = $val;

		return $this;
	}

	function __set($property, $val) {
		if (property_exists($this, $property)) {
			$this->$property = $val;
		}
	}

	function __get($property) {
		if (property_exists($this,$property)) {
			return $this->$property;
		}
	}
}

?>
