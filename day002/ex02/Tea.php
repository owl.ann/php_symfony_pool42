<?php


class Tea extends HotBeverage {

	public $comment;
	public $description;

	public function getComment() {
		return $this->comment;
	}

	public function setComment($val) {
		$this->comment = $val;

		return $this;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription($val) {
		$this->description = $val;

		return $this;
	}
}
