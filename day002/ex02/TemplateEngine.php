<?php

class TemplateEngine {

	private $templateName;
	private $fileName;

	function __construct($value) {
		$this->templateName = $value;
	}

	function createFile(HotBeverage $obj) {
		
		$reflectionObj = new ReflectionClass($obj);
		$this->fileName = $reflectionObj->getName();

		$txt = "";

		if ($this->templateName && $file = fopen($this->templateName, "r")) {
			while ($text = fgets($file)) {
				preg_match_all('/{.*?}/', $text, $matches);
				$matches = $matches[0];

				if (count($matches) > 0) {
					foreach ($matches as $value) {
						if ($value == "{nom}")
							$v = 'name';
						else if ("{prix}" == $value)
							$v = 'price';
						else if ($value == "{resistance}")
							$v = 'resistence';
						else if ($value == "{description}")
							$v = 'description';
						else if ($value == "{commentaire}")
							$v = 'comment';
	
						$txt .= str_replace($value, $obj->$v, $text);
					}
				} else {
					$txt .= $text;
				}
			}
			file_put_contents($this->fileName, $txt);
		}
	}

}

?>
