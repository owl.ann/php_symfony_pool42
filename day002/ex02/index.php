<?php

include ('HotBeverage.php');
include ('Tea.php');
include ('Coffee.php');
include ('TemplateEngine.php');

$hot = new HotBeverage();
$coffee = new Coffee();
$tea = new Tea();

$templateEngine = new TemplateEngine('template.html');

$hot->name = 'Hot Beverage';
$hot->price = 'price';
$hot->resistence = 'resistence';
$templateEngine->createFile($hot);

$coffee->name = 'Coffee';
$coffee->price = 'price';
$coffee->resistence = 'resistence';
$coffee->description = 'description';
$coffee->comment = 'comment';
$templateEngine->createFile($coffee);

$tea->name = 'Tea';
$tea->price = 'price';
$tea->resistence = 'resistence';
$tea->description = 'description';
$tea->comment = 'comment';
$templateEngine->createFile($tea);

?>
