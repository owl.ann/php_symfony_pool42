<?php

class TemplateEngine {

	private $elem;

	function __construct(Elem $elem) {
		$this->elem = $elem;
	}

	function createFile($fileName) {
		
		file_put_contents($fileName, $this->elem->getHTML());

	}

}

?>
