<?php

include ('./Elem.php');
include ('./TemplateEngine.php');
include ('./MyException.php');

$elem = new Elem('html');
$body = new Elem('bodydd');
$body->pushElement(new Elem('p', 'Lorem ipsum', ["style" => "color:pink;", "class" => "text-muted"]));
$elem->pushElement($body);

$template = new TemplateEngine($elem);
$template->createFile("Lorem.html");

?>
