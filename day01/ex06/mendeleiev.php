<?php

function parseName($name) {
	$name[strlen($name) - 1] = "\0";
	return $name;
}

function parsePosition($position) {
	preg_match_all('!\d+!', $position, $matches);
	$position = $matches[0][0];
	return $position;
}

function parseNum($number) {
	preg_match_all('!\d+!', $number, $matches);
	$number = $matches[0][0];
	return $number;
}

function parseMolar($molar) {
	preg_match_all('!\d+\.*\d*!', $molar, $matches);
	$molar = $matches[0][0];
	return $molar;
}

function parseElectron($electron) {
	preg_match_all('!\d+!', $electron[0], $matches);
	$el = $matches[0][0];
	foreach ($electron as $key => $value) {
		if ($key != 0) {
			$el .= " $value";
		}
	}
	return $el;
}

function createCell($name, $position, $small, $number, $molar, $electron) {

	$htmltxt = "";
	if ($position == 0)
		$htmltxt .= "    <tr>\n";

	if ($position == 0 || $position == 1 || $number == 2)
		$htmltxt .= "      <td style=\"background-color:#f37918\">\n";
	else if ($position > 1 && $position < 12)
		$htmltxt .= "      <td style=\"background-color:#75abb3\">\n";
	else
		$htmltxt .= "      <td style=\"background-color:#84689e\">\n";
	$htmltxt .= "        <ul style=\"list-style-type:none;\">\n";
	$htmltxt .= "          <li style=\"text-color: white;\">$number</li>\n";
	$htmltxt .= "          <li style=\"text-color: white;\">$small</li>\n";
	$htmltxt .= "          <h4 style=\"text-color: white; margin: 5px;\">$name</h4>\n";
	$htmltxt .= "          <li style=\"text-color: black;\">$molar</li>\n";
	$htmltxt .= "          <li style=\"text-color: black;\">$electron</li>\n";
	$htmltxt .= "        </ul>\n";
	$htmltxt .= "      </td>\n";

	if ($position == 17)
		$htmltxt .= "    </tr>\n";

	return $htmltxt;
}

function createZeroCell($lastposition, $position) {
	
	$htmltxt = "";

	while (++$lastposition != $position) {
		$htmltxt .= "      <td>&nbsp;</td>\n";
	}
	return $htmltxt;
}

$file = fopen("./ex06.txt", "r");

$htmltxt = "<!DOCTYPE html>\n";
$htmltxt .= "<html>\n";
$htmltxt .= "<head>\n  <title>Mendeleiev</title>\n";
$htmltxt .= "<style>\n" . 
	"td {
		border-radius: 10px;
		text-align: left;
		width: 5%;	
	}\n";
$htmltxt .= "</style>\n";
$htmltxt .= "</head>\n";
$htmltxt .= "<body>\n";
$htmltxt .= "  <h1 style=\"text-align: center;\">Periodic Table of the Elements</h1>\n";
$htmltxt .= "  <table id=\"mendeleieve\" style=\"border-radius: 5px;\">\n";

$lastPosition = 0;
while (($element = fgets($file))) {
	$details = explode(" ", $element);
	$name = $details[0];
	$position = $details[2];
	$number = $details[3];
	$small = $details[5];
	$molar = $details[6];
	$electron = array_slice($details, 7);

	$small = parseName($small);
	$position = parsePosition($position);
	$number = parseNum($number);
	$molar = parseMolar($molar);
	$electron = parseElectron($electron);

	if ($position - $lastPosition > 1)
		$htmltxt .= createZeroCell($lastPosition, $position);
//	 else if ($number == 21 || $number == 39)
//		$htmltxt .= createZeroCell(1, 3);
	$htmltxt .= createCell($name, $position, $small, $number, $molar, $electron);
	$lastPosition = $position;

}

$htmltxt .= "  </table>\n";
$htmltxt .= "</body>\n";
$htmltxt .= "</html>\n";

file_put_contents("./mendeleiev.html", $htmltxt);

?>
