<?php

function capital_city_from($state) {

	$states = [
		'Oregon'=> 'OR',
		'Alabama' => 'AL',
		'New Jersey' => 'NJ',
		'Colorado' => 'CO',
	];

	$capitals = [
		'OR' => 'Salem',
		'AL' => 'Montgomery',
		'NJ' => 'trenton',
		'KS' => 'Topeka',
	];

	if (isset($states[$state])) {
		$state = $states[$state];
		$capital = $capitals[$state];
	} else
		return "Unknown\n";
	return $capital . "\n";
}

?>
