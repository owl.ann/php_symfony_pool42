<?php

function sort_array($array) {
	$result = array();

	$i = 0;
	$max = null;
	$max_age = null;
	while (count($array) > 0) {
		foreach ($array as $name => $age) {
			if ($i++ == 0)
				$max = $name;
			if ($max <= $name) {
				$max = $name;
				$max_age = $age;
			}
		}
		$i = 0;
		$result[$max] = $max_age;
		unset($array[$max]);
	}
	return $result;
}

function array2hash_sorted($array) {

	$result = array();
	$max = 0;
	$min = 0;
	foreach ($array as $key => $value) {
		$name = $value[0];
		$age = $value[1];

		$result[$name] = $age;
	}

	$result = sort_array($result);
	return $result;
}

?>
