<?php

	$states = [
		'Oregon' => 'OR',
		'Alabama' => 'AL',
		'New Jersey' => 'NJ',
		'Colorado' => 'CO',
	];
	$capitals = [
		'OR' => 'Salem',
		'AL' => 'Montgomery',
		'NJ' => 'trenton',
		'KS' => 'Topeka',
	];

function capital_city_from($state) {

	global $states, $capitals;

	if (isset($states[$state])) {
		$state = $states[$state];
		$capital = $capitals[$state];
	} else {
		return "Unknown";
	}
	return $capital;
}

function isCapital($capital) {

	global $states, $capitals;

	if (array_search($capital, $capitals)) {
		$state = array_keys($capitals, $capital)[0];
		if (array_search($state, $states)) {
			$state = array_keys($states, $state)[0];
			return $state;
		}
	}
	return "Unknown";
}

function search_by_states($statesString) {


	$return = "";
	if (isset($statesString)) {
		$arrayOfStates = explode(", ", $statesString);
		foreach ($arrayOfStates as $state) {
			if (($capital = capital_city_from($state)) == "Unknown") {
				if (($realState = isCapital($state)) != "Unknown")
					$return .= $state . " is the capital of $realState.\n";
				else	
					$return .= $state . " is neither a capital nor a state.\n";
			} else {
				$return .= $capital . " is the capital of $state.\n";
			}
		}
	}

	echo $return;

}

?>
